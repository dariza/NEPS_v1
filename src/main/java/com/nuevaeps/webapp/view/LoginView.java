package com.nuevaeps.webapp.view;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nuevaeps.webapp.ral.dto.LoginDTO;
import com.nuevaeps.webapp.ral.dto.ResponseDTO;
import com.nuevaeps.webapp.utils.Configuracion;
import com.nuevaeps.webapp.utils.SeleniumUtils;

public class LoginView {

	@FindBy(id = "usertype")
	private WebElement selectTipoUsuario;

	@FindBy(id = "doctypelogin")
	private WebElement selectTipoDocumento;

	@FindBy(id = "User1")
	private WebElement inputNumeroDocumento;

	@FindBy(id = "InputPassword1")
	private WebElement inputContrasena;

	@FindBy(id = "btnLogin")
	private WebElement btnIngresar;

	@FindBy(id = "txtSWelcom2")
	private WebElement txtBienvenidos;

	public LoginView() {
		SeleniumUtils.esperarElemento("usertype", "id", 30);
		PageFactory.initElements(Configuracion.driver, this);
	}

	public ResponseDTO realizarLogin(LoginDTO loginDTO) {

		try {

			Select tipoUsuario = new Select(selectTipoUsuario);
			tipoUsuario.selectByIndex(Integer.parseInt(loginDTO.getTipoUsuario()));

			Select tipoDocumento = new Select(selectTipoUsuario);
			tipoDocumento.selectByIndex(Integer.parseInt(loginDTO.getTipoDocumento()));

			inputNumeroDocumento.sendKeys(loginDTO.getNumeroDocumento());

			inputContrasena.sendKeys(loginDTO.getContrasena());

			btnIngresar.click();

			String mensajeRespuesta = "En qué podemos ayudarte!!";

			try {
				Alert alertDialog = Configuracion.driver.switchTo().alert();
				mensajeRespuesta = alertDialog.getText();

			} catch (Exception e) {
				mensajeRespuesta = "En qué podemos ayudarte!!";
			}

			return new ResponseDTO(true, mensajeRespuesta);

		} catch (Exception e) {
			return new ResponseDTO(false, e.getMessage());

		}

	}

}
