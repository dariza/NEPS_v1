package com.nuevaeps.webapp.ral.dto;

public class AutorizacionesDTO {

	private String soporte;
	private String observaciones;
	private String radicado;
	private String fechaDesde;
	private String fechaHasta;
	private String ID;
	
	
	public String getSoporte() {
		return soporte;
	}
	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getRadicado() {
		return radicado;
	}
	public void setRadicado(String radicado) {
		this.radicado = radicado;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}

	
}
