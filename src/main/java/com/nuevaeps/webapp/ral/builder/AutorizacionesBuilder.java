package com.nuevaeps.webapp.ral.builder;

import java.util.ArrayList;
import java.util.List;

import com.nuevaeps.webapp.ral.dto.AutorizacionesDTO;

public class AutorizacionesBuilder {
	AutorizacionesDTO SolicitudAutorizacion = new AutorizacionesDTO();

	private List<AutorizacionesDTO> autorizacionesDataPool = new ArrayList<AutorizacionesDTO>();

	public AutorizacionesBuilder(List<AutorizacionesDTO> autorizacionesDataPool) {
		this.autorizacionesDataPool = autorizacionesDataPool;
	}

	public AutorizacionesDTO obtenerAutorizacionFechaMaxima() {
		for (AutorizacionesDTO autorizacionesDTO : autorizacionesDataPool) {
			if (autorizacionesDTO.getID().equals("FECHA_MAXIMA")) {
				
				return autorizacionesDTO;
			}
		}

		return null;
	}

	public AutorizacionesDTO obtenerAutorizacionFechaValida() {
		for (AutorizacionesDTO autorizacionesDTO : autorizacionesDataPool) {
			if (autorizacionesDTO.getID().equals("FECHA_VALIDA")) {
				return autorizacionesDTO;
			}
		}

		return null;
	}

	public AutorizacionesDTO obtenerAutorizacionRadicado() {
		for (AutorizacionesDTO autorizacionesDTO : autorizacionesDataPool) {
			if (autorizacionesDTO.getID().equals("RADICADO")) {
				return autorizacionesDTO;
			}
		}

		return null;
	}

	public AutorizacionesDTO obtenerAutorizacionNulo() {
		for (AutorizacionesDTO autorizacionesDTO : autorizacionesDataPool) {
			if (autorizacionesDTO.getID().equals("NULO")) {
				return autorizacionesDTO;
			}
		}

		return null;
	}

}
