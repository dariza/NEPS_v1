package com.nuevaeps.webapp.bc;

import com.nuevaeps.webapp.ral.dto.LoginDTO;
import com.nuevaeps.webapp.ral.dto.ResponseDTO;
import com.nuevaeps.webapp.view.LoginView;

public class LoginBC {

	public static ResponseDTO realizarLogin(LoginDTO login) {
		return new LoginView().realizarLogin(login);

	}

}
