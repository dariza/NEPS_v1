package com.nuevaeps.webapp.bc;

import com.nuevaeps.webapp.ral.dto.AutorizacionesDTO;
import com.nuevaeps.webapp.ral.dto.ResponseDTO;
import com.nuevaeps.webapp.view.HomeView;

public class AutorizacionesBC {

	public static ResponseDTO consultarAutorizaciones(AutorizacionesDTO autorizaciones) {
		return new HomeView().
		AutorizacionesView(). // aun por probar
		consultarAutorizaciones(autorizaciones);
	}
	
	
//	public static ResponseDTO solicitarAutorizaciones(AutorizacionesDTO autorizaciones) {
//		return new HomeView().
//				ingresarAutorizaciones().
//				solicitarAutorizaciones(autorizaciones);
//	}
	
	public static ResponseDTO consultarAutorizacionesRangoInvalido(AutorizacionesDTO autorizaciones) {
		return new HomeView().
				AutorizacionesView().
				consultarAutorizacionesRangoInvalido(autorizaciones);
	}
}
