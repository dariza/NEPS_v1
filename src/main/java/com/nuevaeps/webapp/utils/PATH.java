package com.nuevaeps.webapp.utils;

public class PATH {
	public static final TipoDriver NAVEGADOR = TipoDriver.CHROME;
	public static final String URL = "https://nepsqa.nuevaeps.com.co/#/";
	public static final String DATA_POOL = "./config/data/DatosLogin.xls";
	public static final String AUTORIZACION_DATA_POOL = "./config/data/Autorizaciones.xls";
}