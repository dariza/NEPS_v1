package com.nuevaeps.webapp.view;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.nuevaeps.webapp.ral.dto.AutorizacionesDTO;
import com.nuevaeps.webapp.ral.dto.ResponseDTO;
import com.nuevaeps.webapp.utils.Configuracion;
import com.nuevaeps.webapp.utils.SeleniumUtils;

public class AutorizacionesView {

	@FindBy(css = ".btn.btn-primary.mt10")
	private WebElement btnSolicitar;

	@FindBy(id = "anexos1")
	private WebElement inputSorporte;

	@FindBy(id = "comentarios")
	private WebElement inputObservaciones;

	@FindBy(id = "btnGrabar")
	private WebElement btnGrabar;

	@FindBy(id = "solictarAutorizaciones")
	private WebElement pantallaSolicitarAutorizaciones;

	@FindBy(id = "radicado")
	private WebElement inputRadicado;

	@FindBy(id = "desde")
	private WebElement inputDesde;

	@FindBy(id = "hasta")
	private WebElement inputHasta;

	@FindBy(css = ".btn.btn-success.mt15") // probar con classname
	private WebElement btnConsultar;

	@FindBy(css = "#consutarForm > div > div:nth-child(5) > button")
	private WebElement btnConsultarAutorizaciones;

	@FindBy(css = "#consutarList > div > h1:nth-child(1)")
	private WebElement RespuestaConsulta;

	public AutorizacionesView() {
		SeleniumUtils.esperarElemento(".btn.btn-primary.mt10", "css", 30);
		PageFactory.initElements(Configuracion.driver, this);
	}

	/**
	 * 
	 * @param autorizaciones
	 * @return
	 */
	public ResponseDTO solicitarAutorizaciones(AutorizacionesDTO autorizaciones) {
		btnSolicitar.click();
		SeleniumUtils.esperarElemento("solictarAutorizaciones", "id", 30);
		/* inputRadicado.sendKeys(autorizaciones.getRadicado()); */
		inputSorporte.sendKeys(autorizaciones.getSoporte());
		inputObservaciones.sendKeys(autorizaciones.getObservaciones());
		btnGrabar.click();

		Alert alertDialog = Configuracion.driver.switchTo().alert();
		String mensajeRespuesta = alertDialog.getText();

		return new ResponseDTO(true, mensajeRespuesta);
	}

	/**
	 * 
	 * @param autorizaciones
	 * @return
	 */
	public ResponseDTO consultarAutorizaciones(AutorizacionesDTO autorizaciones) {

		btnConsultar.click();
		SeleniumUtils.esperarElemento("consutarForm", "id", 30);
		// inputSorporte.sendKeys(autorizaciones.getSoporte());
		inputRadicado.sendKeys(autorizaciones.getRadicado());
		inputDesde.sendKeys(autorizaciones.getFechaDesde());
		inputHasta.sendKeys(autorizaciones.getFechaHasta());
		btnConsultarAutorizaciones.click();
		// SeleniumUtils.esperarElemento("#consutarList > div > h1:nth-child(1)","id",30
		// );//Revision posible error
		String Respuesta = RespuestaConsulta.getText();
		return new ResponseDTO(true, Respuesta);
	}

	public ResponseDTO consultarAutorizacionesRangoInvalido(AutorizacionesDTO autorizaciones) {

		btnConsultar.click();
		SeleniumUtils.esperarElemento("consutarForm", "id", 30);
		// inputSorporte.sendKeys(autorizaciones.getSoporte());
		inputRadicado.sendKeys(autorizaciones.getRadicado());
		inputDesde.sendKeys(autorizaciones.getFechaDesde());
		inputHasta.sendKeys(autorizaciones.getFechaHasta());
		btnConsultarAutorizaciones.click();

		String mensajeRespuesta = "El rango de fechas es máximo de 90 días";

		try {
			Alert alertDialog = Configuracion.driver.switchTo().alert();
			mensajeRespuesta = alertDialog.getText();

		} catch (Exception e) {
			mensajeRespuesta = "no muestra la ventana emergente";
		}
		return new ResponseDTO(true, mensajeRespuesta);

	}

}