package com.nuevaeps.webapp.utils;

public enum TipoDriver {
	CHROME, FIREFOX, IE, SAFARI;
}