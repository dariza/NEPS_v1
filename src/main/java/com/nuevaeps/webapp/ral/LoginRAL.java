package com.nuevaeps.webapp.ral;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.nuevaeps.webapp.ral.dto.LoginDTO;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * <pre>
 * Fecha      Autor     
 * 17-11-2018 Dilan Steven Mejia
 * </pre>
 * 
 * 
 * @author Dilan Steven Mejia
 * @version 1.0
 * @category DAL
 **/

public class LoginRAL {

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 17-11-2018 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Hidrata el login con cada una de las filas de un excel.
	 * 
	 * @author Dilan Steven Mejia
	 * @return Lista de reservas
	 * 
	 **/
	public static ArrayList<LoginDTO> obtenerDatosLogin(String dataPool) {
		ArrayList<LoginDTO> listLoginDTO = new ArrayList<LoginDTO>();
		try {
			Workbook excelDataPool = Workbook.getWorkbook(new File(dataPool));
			Sheet hojaExcelDataPool = excelDataPool.getSheet(0);

			for (int fila = 1; fila < hojaExcelDataPool.getRows(); fila++) {
				LoginDTO login = new LoginDTO();

				login.setTipoUsuario(validarDato(hojaExcelDataPool, "tipoUsuario", fila));
				login.setTipoDocumento(validarDato(hojaExcelDataPool, "tipoDocumento", fila));
				login.setNumeroDocumento(validarDato(hojaExcelDataPool, "numeroDocumento", fila));
				login.setContrasena(validarDato(hojaExcelDataPool, "contrasena", fila));
				login.setID(validarDato(hojaExcelDataPool, "ID", fila));

				listLoginDTO.add(login);

			}

		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listLoginDTO;
	}

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 17-11-2018 Dilan Steven Mejia
	 * 
	 * </pre>
	 * 
	 * Valida que si hay un elemento en la celda, sino retorna vacio.
	 * 
	 * @author Dilan Steven Mejia
	 * @return Elemento encontrado en la celda
	 * 
	 **/

	public static String validarDato(Sheet hojaExcelDataPool, String campo, int fila) {
		try {
			return hojaExcelDataPool.getCell(hojaExcelDataPool.findCell(campo).getColumn(), fila).getContents();

		} catch (Exception e) {

			return "";
		}

	}

}
