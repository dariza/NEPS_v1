package com.nuevaeps.webapp.ral.builder;

import java.util.ArrayList;
import java.util.List;

import com.nuevaeps.webapp.ral.dto.LoginDTO;

public class LoginBuilder {
	LoginDTO login = new LoginDTO();

	private List<LoginDTO> loginDataPool = new ArrayList<LoginDTO>();

	public LoginBuilder(List<LoginDTO> loginDataPool) {
		this.loginDataPool = loginDataPool;
	}

	public LoginDTO loginExitoso() {
		for (LoginDTO login : loginDataPool) {
			if (login.getID().equals("EXITOSO")) {
				return login;
			}
		}

		return null;
	}

	public LoginDTO loginUsuarioInvalido() {
		for (LoginDTO login : loginDataPool) {
			if (login.getID().equals("USUARIO_ERRONEO")) {
				return login;
			}
		}
		return null;
	}

	public LoginDTO loginContrasenaInvalida() {
		for (LoginDTO login : loginDataPool) {
			if (login.getID().equals("CONTRASENA_ERRONEA")) {
				return login;
			}
		}
		return null;
	}
}
