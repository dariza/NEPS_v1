package com.nuevaeps.webapp.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nuevaeps.webapp.bc.AutorizacionesBC;
import com.nuevaeps.webapp.bc.LoginBC;
import com.nuevaeps.webapp.ral.AutorizacionesRAL;
import com.nuevaeps.webapp.ral.LoginRAL;
import com.nuevaeps.webapp.ral.dto.AutorizacionesDTO;
import com.nuevaeps.webapp.ral.builder.AutorizacionesBuilder;
import com.nuevaeps.webapp.ral.dto.LoginDTO;
import com.nuevaeps.webapp.ral.dto.ReporteDTO;
import com.nuevaeps.webapp.utils.Configuracion;
import com.nuevaeps.webapp.utils.PATH;

import jxl.read.biff.BiffException;

/**
 * <pre>
 * Fecha      Autor     
 * 22-12-2021 Dilan Steven Mejia
 * </pre>
 * 
 * Ejecuta los casos de prueba para realizar Solicitar y Consultar
 * Autorizaciones.
 * 
 * @author Dilan Steven Mejia
 * 
 **/
public class AutorizacionesTest {

	ArrayList<AutorizacionesDTO> AutorizacionesDataPool = new ArrayList<AutorizacionesDTO>();
	ArrayList<LoginDTO> loginDataPool = new ArrayList<LoginDTO>();
	//private ArrayList<AutorizacionesDTO> AutorizacionDataPool;

	
	@Before
	public void setup() {
		Configuracion.iniciarConfiguracion();// check
		this.loginDataPool = LoginRAL.obtenerDatosLogin(PATH.DATA_POOL);// check
		this.AutorizacionesDataPool = AutorizacionesRAL.obtenerDatosAutorizaciones(PATH.AUTORIZACION_DATA_POOL);// check
		doLogin();
	}

	public void doLogin() {
		/*
		 * posible error de apertura cde varias ventanas al tiempo
		 * 
		 */

		for (LoginDTO login : loginDataPool) {
			/* Enviar cada objeto que contiene el escenario */
			LoginBC.realizarLogin(login);
		}
	}

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 22-12-2021 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Ejecuta los casos de prueba para realizar Solicitar y Consultar
	 * Autorizaciones.
	 * 
	 * @author Dilan Steven Mejia
	 * @throws BiffException 
	 * 
	 **/

	@Test
	public void consultaAutorizacionesFechaMaxima() throws BiffException {
		new AutorizacionesBuilder(AutorizacionesDataPool);
		// arrange
		AutorizacionesBuilder autorizacionesBuilder = new AutorizacionesBuilder(AutorizacionesDataPool);
		AutorizacionesDTO autorizacionFechaMaxima = autorizacionesBuilder.obtenerAutorizacionFechaMaxima();
		String mensajeAutorizacion = "";
		String mensajeAutorizacionEsperado = "El rango de fechas es máximo de 90 días";
		
		// act
		mensajeAutorizacion = AutorizacionesBC.consultarAutorizacionesRangoInvalido(autorizacionFechaMaxima).getMensajePantalla();
		System.out.println(mensajeAutorizacionEsperado);
		// assert
		assertEquals(mensajeAutorizacion, mensajeAutorizacionEsperado);
//		stopAllDrivers();
		new ReporteDTO("Reporte_Autorizaciones.xls");
	}


	@Test
	public void consultaAutorizacionesFechaValida() throws BiffException {
		new AutorizacionesBuilder(AutorizacionesDataPool);
		// arrange
		AutorizacionesBuilder autorizacionesBuilder = new AutorizacionesBuilder(AutorizacionesDataPool);
		AutorizacionesDTO autorizacionFechaValida = autorizacionesBuilder.obtenerAutorizacionFechaValida();
		String mensajeAutorizacion = "";
		String mensajeAutorizacionEsperado = "Consultar autorizaciones";
		// act
		mensajeAutorizacion = AutorizacionesBC.consultarAutorizaciones(autorizacionFechaValida).getMensajePantalla();

		// assert
		assertEquals(mensajeAutorizacion, mensajeAutorizacionEsperado);
//		stopAllDrivers();
		new ReporteDTO("Reporte_Autorizaciones.xls");
	}

	@Test
	public void consultaAutorizacionesNulo() throws BiffException {
		new AutorizacionesBuilder(AutorizacionesDataPool);
		// arrange
		AutorizacionesBuilder autorizacionesBuilder = new AutorizacionesBuilder(AutorizacionesDataPool);
		AutorizacionesDTO autorizacionNulo = autorizacionesBuilder.obtenerAutorizacionNulo();
		String mensajeAutorizacion = "";
		String mensajeAutorizacionEsperado = "Debe seleccionar un rango de fechas o escribir un número de radicado";
		// act
		mensajeAutorizacion = AutorizacionesBC.consultarAutorizacionesRangoInvalido(autorizacionNulo)
				.getMensajePantalla();

		// assert
		assertEquals(mensajeAutorizacion, mensajeAutorizacionEsperado);
//		stopAllDrivers();
		new ReporteDTO("Reporte_Autorizaciones.xls");
	}

	@Test
	public void consultaAutorizacionesRadicado() throws BiffException {
		// new AutorizacionesBuilder(AutorizacionesDataPool);

		// arrange
		AutorizacionesBuilder autorizacionesBuilder = new AutorizacionesBuilder(AutorizacionesDataPool);
		AutorizacionesDTO autorizacionRadicado = autorizacionesBuilder.obtenerAutorizacionRadicado();
		String mensajeAutorizacion = "";
		String mensajeAutorizacionEsperado = "Consultar autorizaciones";
		// act
		mensajeAutorizacion = AutorizacionesBC.consultarAutorizaciones(autorizacionRadicado).getMensajePantalla();

		// assert
		assertEquals(mensajeAutorizacion, mensajeAutorizacionEsperado);
//		stopAllDrivers();
		new ReporteDTO("Reporte_Autorizaciones.xls");
	}
	
	// Se cierran las sesiones del driver.
	@After
		public void stopAllDrivers() {
			Configuracion.driver.quit();
		}


}
