package com.nuevaeps.webapp.test;

import org.junit.runner.JUnitCore;

public class TestRunner {

	public static void main(String[] args) {
		String bateriaPruebas = args[0];
		
		switch (bateriaPruebas) {
		case "LOGIN":
			JUnitCore.main("com.nuevaeps.webapp.test.LoginTest");
			break;
		case "AUTORIZACIONES":
			JUnitCore.main("com.nuevaeps.webapp.test.AutorizacionesTest");
			break;
		default:
			break;
		}
		
		
		
	}
}
