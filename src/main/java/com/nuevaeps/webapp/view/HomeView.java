package com.nuevaeps.webapp.view;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//import com.nuevaeps.webapp.bc.AutorizacionesBC;
import com.nuevaeps.webapp.utils.Configuracion;
import com.nuevaeps.webapp.utils.SeleniumUtils;

public class HomeView {

	@FindBy(css="[href='#/Authorizations']")
	private  WebElement btnAutorizaciones;
	
	@FindBy(className = ".btn.btn-success.mt15")//duplicadoe en homeview
	private WebElement btnConsulta;
	
	

	public HomeView() {
		SeleniumUtils.esperarElemento("[href='#/Authorizations']","css",30);
		PageFactory.initElements(Configuracion.driver, this);
	}


	public AutorizacionesView AutorizacionesView() {
		btnAutorizaciones.click();
		return new AutorizacionesView();

	}
	
	public AutorizacionesView ConsultarAutorizacionView() {
		btnConsulta.click();
		return new AutorizacionesView();
	}


//	public AutorizacionesBC ingresarAutorizaciones() {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	


}
