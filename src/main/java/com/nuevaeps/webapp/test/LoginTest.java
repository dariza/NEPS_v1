package com.nuevaeps.webapp.test;

import static org.junit.Assert.assertEquals;

import java.awt.Label;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nuevaeps.webapp.bc.LoginBC;
import com.nuevaeps.webapp.ral.LoginRAL;
import com.nuevaeps.webapp.ral.builder.LoginBuilder;
import com.nuevaeps.webapp.ral.dto.LoginDTO;
import com.nuevaeps.webapp.ral.dto.ReporteDTO;
import com.nuevaeps.webapp.utils.Configuracion;
import com.nuevaeps.webapp.utils.PATH;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * <pre>
 * Fecha      Autor     
  * 22-12-2021 Dilan Steven Mejia
 * </pre>
 * 
 * Ejecuta los casos de prueba del login.
 * 
 * @author Dilan Steven Mejia
 * @version 1.0
 * @category Test
 **/

public class LoginTest {

	ArrayList<LoginDTO> loginDataPool = new ArrayList<LoginDTO>();

	// Ingresa la configuracion del navegador,la url, y la ruta del archivo de la
	// data
	@Before
	public void setup() {
		Configuracion.iniciarConfiguracion();
		this.loginDataPool = LoginRAL.obtenerDatosLogin(PATH.DATA_POOL);
	}

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 22-12-2021 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Ejecuta el caso de prueba con las credenciales de un usuario incorrectas.
	 * 
	 * @author Dilan Steven Mejia
	 * @throws BiffException 
	 * 
	 **/
	@Test
	public void realizarLoginUsuarioInvalido() throws BiffException {

		new LoginBuilder(loginDataPool);

		// arrange
		LoginBuilder loginBuilder = new LoginBuilder(loginDataPool);
		LoginDTO loginUsuarioInvalido = loginBuilder.loginUsuarioInvalido();
		String mensajeUsuario = "";
		String mensajeEsperado = "Usuario o clave incorrecto";
		// act
		mensajeUsuario = LoginBC.realizarLogin(loginUsuarioInvalido).getMensajePantalla();
		// assert
		assertEquals(mensajeUsuario, mensajeEsperado);
//		stopAllDrivers(); //->>> no lo cierra cokm after ni con orden... buscar
		new ReporteDTO("Reporte_Login.xls");

	}

	/**
	 * <pre>
	 * Fecha      Autor     
	  * 22-12-2021 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Ejecuta el caso de prueba con un usuario correcto y una contraseña
	 * incorrecta.
	 * 
	 * @author Dilan Steven Mejia
	 * @throws BiffException 
	 * 
	 **/
	@Test
	public void realizarLoginContrasenaInvalida() throws BiffException {
		new LoginBuilder(loginDataPool);

		// arrange
		LoginBuilder loginBuilder = new LoginBuilder(loginDataPool);
		LoginDTO loginContrasenaInvalida = loginBuilder.loginContrasenaInvalida();
		String mensajeUsuario = "";
		String mensajeEsperado = "Usuario o clave incorrecto";
		// act
		mensajeUsuario = LoginBC.realizarLogin(loginContrasenaInvalida).getMensajePantalla();
		// assert
		assertEquals(mensajeUsuario, mensajeEsperado);
//		stopAllDrivers(); //liena 68
		new ReporteDTO("Reporte_Login.xls");
	}

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 22-12-2021 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Ejecuta el caso de prueba con un usuario correcto y contraseña correcta.
	 * 
	 * @author Dilan Steven Mejia
	 * @throws BiffException 
	 * 
	 **/
	@Test
	public void realizarLoginUsuarioValido() throws BiffException {
		new LoginBuilder(loginDataPool);

		// arrange
		LoginBuilder loginBuilder = new LoginBuilder(loginDataPool);
		LoginDTO loginExitoso = loginBuilder.loginExitoso();
		String mensajeUsuario = "";
		String mensajeEsperado = "En qué podemos ayudarte!!";
		// act
		//mensajeUsuario = LoginBC.realizarLoginValido(loginExitoso).getMensajePantalla();
		mensajeUsuario = LoginBC.realizarLogin(loginExitoso).getMensajePantalla();
		// assert
		assertEquals(mensajeUsuario, mensajeEsperado);
//		stopAllDrivers(); //linea 68
		new ReporteDTO("Reporte_Login.xls");
	}

	// Se cierran las sesiones del driver.
	@After
	public void stopAllDrivers() {
		Configuracion.driver.quit();
		// implementar el reporte ..
	}

}
