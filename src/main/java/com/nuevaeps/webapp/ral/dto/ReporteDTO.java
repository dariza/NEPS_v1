package com.nuevaeps.webapp.ral.dto;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ReporteDTO {
	public ReporteDTO(String sitio) throws BiffException {
		// TODO Auto-generated method stub
		WritableWorkbook libroSalida;
		Workbook libroSalida1;
		WritableSheet hojaSalida;
		String 	rutaarchivoparametro = System.getProperty("user.dir");

		try {
						
//			libroSalida = Workbook.createWorkbook(new File(rutaarchivoparametro+"\\Resultado_Login.xls")); //resultadologin resultados aturizaciones)
//			libroSalida1 = new FileInputStream(rutaarchivoparametro+"\\Resultado_Login.xls");
			
			File archivo = new File(rutaarchivoparametro+"\\config\\data\\"+sitio); 
			if (!archivo.exists()) {
				libroSalida = Workbook.createWorkbook(new File(rutaarchivoparametro+"\\config\\data\\"+sitio)); //resultadologin resultados aturizaciones)
				hojaSalida = libroSalida.createSheet("Hoja1", 0);// defino la hoja y el indice----.probar con otro numero
				
			}else {
				libroSalida1 = Workbook.getWorkbook(new File(rutaarchivoparametro+"\\config\\data\\"+sitio));
				libroSalida = Workbook.createWorkbook(new File(rutaarchivoparametro+"\\config\\data\\"+sitio),libroSalida1);
				
			}
			
			
			hojaSalida = libroSalida.getSheet(0);
			
			String fecha = (DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm").format(LocalDateTime.now())).toString();
			int numRow = hojaSalida.getRows();
			hojaSalida.addCell(new jxl.write.Label(0,numRow,fecha));
			hojaSalida.addCell(new jxl.write.Label(1,numRow,"Exitoso"));
			
			libroSalida.write();
			libroSalida.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	public static void main(String[] args) throws BiffException {
//		new ReporteDTO("Resultado_Login.xls");
//	}
}
