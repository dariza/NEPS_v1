package com.nuevaeps.webapp.ral;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.nuevaeps.webapp.ral.dto.AutorizacionesDTO;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * <pre>
 * Fecha      Autor     
 * 11-01-2021 David Ariza
 * </pre>
 * 
 * 
 * @author David Ariza
 * @version 1.0
 * @category DAL
 **/

public class AutorizacionesRAL {

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 17-11-2018 Dilan Steven Mejia
	 * </pre>
	 * 
	 * Hidrata las autorizaciones con cada una de las filas de un excel.
	 * 
	 * @author Dilan Steven Mejia
	 * @return Lista de reservas
	 * 
	 **/
	public static ArrayList<AutorizacionesDTO> obtenerDatosAutorizaciones(String dataPool) {
		ArrayList<AutorizacionesDTO> listAutorizacionesDTO = new ArrayList<AutorizacionesDTO>();
		try {
			Workbook excelDataPool = Workbook.getWorkbook(new File(dataPool));
			Sheet hojaExcelDataPool = excelDataPool.getSheet(0);

			for (int fila = 1; fila < hojaExcelDataPool.getRows(); fila++) {
				AutorizacionesDTO autorizacion = new AutorizacionesDTO();

				autorizacion.setRadicado(validarDato(hojaExcelDataPool, "NumeroAutorizacion", fila));
				autorizacion.setFechaDesde(validarDato(hojaExcelDataPool, "FechaDesde", fila));
				autorizacion.setFechaHasta(validarDato(hojaExcelDataPool, "FechaHasta", fila));
				autorizacion.setID(validarDato(hojaExcelDataPool, "id", fila));
				listAutorizacionesDTO.add(autorizacion);

			}

		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listAutorizacionesDTO;
	}

	/**
	 * <pre>
	 * Fecha      Autor     
	 * 17-11-2018 Dilan Steven Mejia
	 * 
	 * </pre>
	 * 
	 * Valida que si hay un elemento en la celda, sino retorna vacio.
	 * 
	 * @author Dilan Steven Mejia
	 * @return Elemento encontrado en la celda
	 * 
	 **/

	public static String validarDato(Sheet hojaExcelDataPool, String campo, int fila) {
		try {
			return hojaExcelDataPool.getCell(hojaExcelDataPool.findCell(campo).getColumn(), fila).getContents();

		} catch (Exception e) {

			return "";
		}

	}

}
